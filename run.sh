#!/bin/bash

source src/common.sh
source src/steering.sh
source src/tank.sh
source src/dashboard.sh
source src/webserver.sh

help_text () {
  echo "Usage: $0 [-s HTTP_SCHEMA] COMMAND

Options:
  -s HTTP_SCHEMA    'http' or 'https'. Overwrites automatic detection on prod/dev mode setting.
                    Only use this if you really know what you are doing. Data privacy may be at risk
  -h                Print this text and quit.

Use one of the following COMMANDs:
  init         Initialise the system before the first start. Does all the necessary setup.
  start        Start all AuRa components (should be initialised first)
  stop         Stop all AuRa components
  cleanup      Clear all containers and images (Attention: will destroy any saved data)
  rebuild      Rebuilds all component images
  help         Print this text. Similar to option -h.
"
}
help_text_and_quit () {
  help_text
  exit
}

# TODO: create getopts interface with options to init, run and stop services
while getopts ":hs:" opt; do
  case $opt in
    s)
      if [ "$OPTARG" != "http" -a "$OPTARG" != "https" ]; then
        echo "Invalid argument for option -s: $OPTARG" >&2
        echo "Can only be either http or https." >&2
        exit 1
      fi
      OVERWRITE_HTTP_SCHEMA="$OPTARG"
      ;;
    h)
      help_text_and_quit
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      echo
      help_text_and_quit
      ;;
  esac
done

COMMAND=${!OPTIND}
case $COMMAND in
  init)
    read_config
    init_common
    start_database_containers
    # TODO: poll until databases are ready instead of sleeping 5s
    echo "Sleeping for 5 seconds for the db containers to be fully set up"
    sleep 5
    init_steering
    init_tank
    init_dashboard
    ;;
  start)
    start_containers
    read_config
    init_webserver
    start_webserver
    ;;
  stop)
    docker-compose down
    ;;
  cleanup)
    echo "TODO: implemented this!"
    ;;
  rebuild)
    echo "TODO: implemented this!"
    ;;
  help)
    help_text_and_quit
    ;;
  *)
    if [ -z "$COMMAND" ]; then
      help_text_and_quit
    fi
    echo "Unknown command: $COMMAND"
    exit 1
esac

