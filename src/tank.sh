init_tank () {
  echo "Create new tank.yaml from sample file"
  cp container-config/tank.sample.yaml container-config/tank.yaml
  echo "Write OIDC info to tank.yaml"
  docker run --rm -v "${PWD}"/container-config/tank.yaml:/workdir/tank.yaml -u $UID mikefarah/yq eval ".auth.oidc.client-id = \"$TANK_CLIENT_ID\"" -i tank.yaml
  docker run --rm -v "${PWD}"/container-config/tank.yaml:/workdir/tank.yaml -u $UID mikefarah/yq eval ".auth.oidc.client-secret = \"$TANK_CLIENT_SECRET\"" -i tank.yaml
  docker run --rm -v "${PWD}"/container-config/tank.yaml:/workdir/tank.yaml -u $UID mikefarah/yq eval ".auth.oidc.callback-url = \"$HTTP_SCHEMA://$AURA_DOMAIN/tank/auth/oidc/callback\"" -i tank.yaml
}
