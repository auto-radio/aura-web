# read_config
#   use the yq container to read all relevant settings from aura-config.yaml
#   and set shell variables accordingly
read_config () {
  echo "Reading required variables from aura-config.yaml"
  MODE="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.mode' aura-config.yaml)"
  # by default https should be used except for local dev mode
  # TODO: create getopts options to override this setting for testing purposes
  if [ "$MODE" = "dev" ]; then
    HTTP_SCHEMA="http"
  else
    HTTP_SCHEMA="https"
  fi
  AURA_DOMAIN="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.domain' aura-config.yaml)"
  USERNAME="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.admin.username' aura-config.yaml)"
  USERMAIL="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.admin.email' aura-config.yaml)"
  DEBUG="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.debug' aura-config.yaml)"
  echo "Reading steering database config from aura-config.yaml"
  STEERING_DB_PASS="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.steering.db_pass' aura-config.yaml)"
  STEERING_DB_USER="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.steering.db_user' aura-config.yaml)"
  STEERING_DB_NAME="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.steering.db_name' aura-config.yaml)"
  echo "Reading tank database config from aura-config.yaml"
  TANK_DB_PASS="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.tank.db_pass' aura-config.yaml)"
  TANK_DB_USER="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.tank.db_user' aura-config.yaml)"
  TANK_DB_NAME="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.tank.db_name' aura-config.yaml)"
  echo "Reading dashboard config from aura-config.yaml"
  VUE_APP_API_STEERING_OIDC_EXPIRE_NOTIFICATION="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.dashboard.VUE_APP_API_STEERING_OIDC_EXPIRE_NOTIFICATION' aura-config.yaml)"
  VUE_APP_TIMESLOT_FILTER_DEFAULT_NUMSLOTS="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.dashboard.VUE_APP_TIMESLOT_FILTER_DEFAULT_NUMSLOTS' aura-config.yaml)"
  VUE_APP_SHOW_THRESHOLD="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.dashboard.VUE_APP_SHOW_THRESHOLD' aura-config.yaml)"
  VUE_APP_TIMESLOT_FILTER_DEFAULT_DAYS="$(docker run --rm -v "${PWD}"/aura-config.yaml:/workdir/aura-config.yaml mikefarah/yq eval '.aura.dashboard.VUE_APP_TIMESLOT_FILTER_DEFAULT_DAYS' aura-config.yaml)"
}

# init_common
#   write the global .env file for docker-compose with relevant variables
init_common () {
  echo "Writing global .env for docker-compose"
  echo "
STEERING_DB_PASS=$STEERING_DB_PASS
STEERING_DB_USER=$STEERING_DB_USER
STEERING_DB_NAME=$STEERING_DB_NAME
TANK_DB_PASS=$TANK_DB_PASS
TANK_DB_USER=$TANK_DB_USER
TANK_DB_NAME=$TANK_DB_NAME
" > .env
}

start_database_containers () {
  echo "Starting database containers"
  docker-compose up -d steering-postgres tank-postgres
}

start_containers () {
  echo "Starting steering and tank containers, building dashboard production build"
  docker-compose up -d steering tank dashboard
}
