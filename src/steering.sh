# get_config_steering $1
#   outputs the value of a key, if set
get_config_steering () {
  grep "^$1" steering/.env | cut -d'=' -f2
}

# set_config_steering $1 $2
#   checks the steering/.env file and updates the key $1 to value $2
#   keys are only considered if the are found on the beginning of a line
#   if the key does not exist, a new line will be added, otherwise
#   the existing line will be overwritten
set_config_steering () {
  if [ -z "$(get_config_steering "$1")" ]; then
    echo "$1=$2" >> steering/.env
  else
    sed -i "s/^$1=.*$/$1=$2/" steering/.env
  fi
}

# init_steering
#   sets all needed steering/.env values and starts steering
#   then initializes the steering database, creates a superuser and
#   creates the OpenID Connect clients needed for the tank and dashboard config
init_steering () {
  echo "Generating settings for steering/.env"
  if [ ! -f steering/.env ]; then touch steering/.env; fi
  # we only want to generate a new secret key, if there isn't already one there
  if [ -z "$(get_config_steering SECRET_KEY)" ]; then
    set_config_steering SECRET_KEY "$(tr -dc A-Z-a-z-0-9 < /dev/urandom | head -c32)"
  fi
  set_config_steering DBPASS $STEERING_DB_PASS
  set_config_steering RUN_IN_DOCKER True
  if [ "$DEBUG" = "true" ]; then
    set_config_steering DEBUG True
  fi
  # django has to accept our configured domain as well as steering (due to proxying)
  # TODO: localhost only makes sense if the container ports are mapped (usually in a dev setup)
  set_config_steering ALLOWED_HOSTS "127.0.0.1,localhost,steering,$AURA_DOMAIN"
  echo "Building steering image"
  docker-compose build steering
  echo "Starting steering container"
  docker-compose up -d steering
  echo "Running migrations"
  docker exec steering python manage.py migrate
  echo "Loading fixtures"
  docker exec steering sh -c 'python manage.py loaddata fixtures/*/*.json'
  # TODO: only create user if not already set (or if explicitly demanded)
  echo "Creating steering superuser account for $USERNAME <$USERMAIL>."
  echo "Please provide a (strong) password."
  docker exec -it steering python manage.py createsuperuser --username "$USERNAME" --email "$USERMAIL"
  echo "Creating RSA key for OpenID Connect"
  docker exec steering python manage.py creatersakey
  echo "Creating OIDC client for dashboard"
  DASHBOARD_CLIENT_ID="$(docker exec steering python manage.py create_oidc_client -r "id_token token" --no-require-consent -i -u "$HTTP_SCHEMA://$AURA_DOMAIN/oidc_callback.html" -u "$HTTP_SCHEMA://$AURA_DOMAIN/oidc_callback_silentRenew.html" -p "$HTTP_SCHEMA://$AURA_DOMAIN" dashboard public)"
  echo "Creating OIDC client for tank"
  TANK_OIDC_DETAILS="$(docker exec steering python manage.py create_oidc_client -r "code" -i -u "$HTTP_SCHEMA://$AURA_DOMAIN/tank/auth/oidc/callback" -p "$HTTP_SCHEMA://$AURA_DOMAIN" tank confidential)"
  TANK_CLIENT_ID="$(echo $TANK_OIDC_DETAILS | cut -d ' ' -f 1)"
  TANK_CLIENT_SECRET="$(echo $TANK_OIDC_DETAILS | cut -d ' ' -f 2)"
  echo "Stopping steering"
  docker-compose down
}
