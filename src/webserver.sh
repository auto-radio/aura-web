init_webserver () {
  # only in defined modes like "dev" we don't use TLS
  if [ "$MODE" = "dev" ]; then
    cp container-config/nginx.dev-sample.conf container-config/nginx.conf
  # in all other cases we want to be on the safe side and enable TLS
  else
    echo "Starting initial web container to obtain Let's Encrypt cert"
    cp container-config/nginx.initial-sample.conf container-config/nginx.conf
    sed -i "s/sample\\.example\\.com/$AURA_DOMAIN/" container-config/nginx.conf
    docker-compose up -d aura-web
    # TODO: check whether we already have a valid cert
    docker exec aura-web certbot certonly --webroot -w /usr/share/nginx/html -d "$AURA_DOMAIN" -m "$USERMAIL" --agree-tos --non-interactive
    docker-compose stop aura-web
    echo "Creating final TLS enabled nginx conf"
    cp container-config/nginx.full-sample.conf container-config/nginx.conf
  fi
  sed -i "s/sample\\.example\\.com/$AURA_DOMAIN/" container-config/nginx.conf
}

start_webserver () {
  echo "Start nginx container as web proxy"
  docker-compose up -d aura-web
}
