# get_config_dashboard $1 [$2]
#   outputs the value of a key, if set in .env.production
#   if the parameter $2 is used and set to "dev" the .env.development file will be used
get_config_dashboard () {
  if [ "$2" = "dev" ]; then
    DASHBOARD_ENV="development"
  else
    DASHBOARD_ENV="production"
  fi
  grep -oP "^$1 = ?\K.*$" dashboard/.env.$DASHBOARD_ENV
}

# set_config_dashboard $1 $2 [$3]
#   checks the dashboard/.env.production file and updates the key $1 to value $2
#   keys are only considered if the are found on the beginning of a line
#   if the key does not exist, a new line will be added, otherwise
#   the existing line will be overwritten
#   if the optional parameter $3 is used and set to "dev" the operations will be
#   performed on the dashboard/.env.development file
set_config_dashboard () {
  if [ "$3" = "dev" ]; then
    DASHBOARD_ENV="development"
  else
    DASHBOARD_ENV="production"
  fi
  SETTING_FOUND="false"
  grep -q "$1" dashboard/.env.$DASHBOARD_ENV
  if [ $? -eq 0 ]; then SETTING_FOUND="true"; fi
  if [ -z "$(get_config_dashboard "$1" "$3")" -a $SETTING_FOUND = "false" ]; then
    echo "$1 = $2" >> dashboard/.env.$DASHBOARD_ENV
  else
    VALUE="$2"
    sed -i "s/^$1 =.*$/$1 = ${VALUE//\//\\/}/" dashboard/.env.$DASHBOARD_ENV
  fi
}

init_dashboard () {
  cp dashboard/sample.env.production dashboard/.env.production
  # TODO: set .env.production variables according to aura config and oidc client"
  echo "Setting dashboard config values"
  set_config_dashboard VUE_APP_BASEURI_STEERING "$HTTP_SCHEMA://$AURA_DOMAIN"
  set_config_dashboard VUE_APP_BASEURI_MEDIA "$HTTP_SCHEMA://$AURA_DOMAIN/steering/site_media"
  set_config_dashboard VUE_APP_API_STEERING "$HTTP_SCHEMA://$AURA_DOMAIN/steering/api/v1/"
  set_config_dashboard VUE_APP_API_STEERING_SHOWS "$HTTP_SCHEMA://$AURA_DOMAIN/steering/api/v1/shows/"
  set_config_dashboard VUE_APP_API_TANK "$HTTP_SCHEMA://$AURA_DOMAIN/tank/api/v1/"
  set_config_dashboard VUE_APP_TANK "$HTTP_SCHEMA://$AURA_DOMAIN/tank/"
  set_config_dashboard VUE_APP_OIDC_CLIENT_ID "$DASHBOARD_CLIENT_ID"
  set_config_dashboard VUE_APP_API_STEERING_OIDC_URI "$HTTP_SCHEMA://$AURA_DOMAIN/openid"
  set_config_dashboard VUE_APP_API_STEERING_OIDC_EXPIRE_NOTIFICATION "$VUE_APP_API_STEERING_OIDC_EXPIRE_NOTIFICATION"
  set_config_dashboard VUE_APP_API_STEERING_OIDC_REDIRECT_URI "$HTTP_SCHEMA://$AURA_DOMAIN/oidc_callback.html"
  set_config_dashboard VUE_APP_API_STEERING_OIDC_REDIRECT_URI_SILENT "$HTTP_SCHEMA://$AURA_DOMAIN/oidc_callback_silentRenew.html"
  set_config_dashboard VUE_APP_API_STEERING_OIDC_REDIRECT_URI_POSTLOGOUT "$HTTP_SCHEMA://$AURA_DOMAIN"
  set_config_dashboard VUE_APP_TIMESLOT_FILTER_DEFAULT_NUMSLOTS "$VUE_APP_TIMESLOT_FILTER_DEFAULT_NUMSLOTS"
  set_config_dashboard VUE_APP_SHOW_THRESHOLD "$VUE_APP_SHOW_THRESHOLD"
  set_config_dashboard VUE_APP_TIMESLOT_FILTER_DEFAULT_DAYS "$VUE_APP_TIMESLOT_FILTER_DEFAULT_DAYS"
}
