# aura-web [WIP]

This is a work in progress repository aimed to create an easy initial setup for the AuRa web components.
When everything is working smooth, it should be integrated into the meta repo.


# Prerequisites

* Install docker and docker-compose


# Setup

Clone aura-web: (TODO: improve description)

After you have cloned the aura-web repository, change into it (`cd aura-web`) and
initialise the sub repositories:

```bash
git submodule init
git submodule update
```

Then create a copy of the _aura-config.sample.yaml_ file to _aura-config.yaml_ and
adapt the values according to your setup. For local dev environments see notes below.

Now before your first start do the setup by calling:

```bash
./run.sh init
```

In the setup process you will be asked to enter you password for the steering component.
This is for your administrator account (with the username that you chose in the _aura-config.yaml_).

After the setup you can start up all components: 

```bash
./run.sh start
```

You can now visit the dashboard on the domain you have set in _aura-config.yaml_.
To access the steering backend directly for management purposes add a _/steering/admin_
after the domain.

To stop all components do:

```bash
./run.sh stop
```

This only stops and destroys the containers, but all persistent data (e.g. user data in the
databases), will not be touched. So next time you can just do `./run.sh start` again
to start up fresh containers.


## TODOs:

* implement cleanup and rebuild commands and add description above
* only build dashboard after init and on rebuild
* implement dev mode
  - implment the use of OVERRIDE_HTTP_SCHEMA
* refactor the submodules into the individual run.sh scripts of the components
* add note on `docker-compose logs -f` (or create run.sh command)

## To discuss:

* should we fully build the dashboard before starting the webserver?
  - atm. the webserver already starts while dashboard is building. a fast user
    might visit the frontend with an old build and get an OIDC error, because in
    the original build there was another OIDC client ID


### Common pitfalls to elaborate

* Write permission of the container-data/* directories, if they are not created by the containers themselves


# Notes for devs

TODO
 